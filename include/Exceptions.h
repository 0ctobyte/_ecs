// Sekhar Bhattacharya

#ifndef __EXCEPTIONS__
#define __EXCEPTIONS__

#include <exception>
#include <string>

#include <SDL_error.h>

class _GeneralException : public std::exception {
public:
	_GeneralException() throw() {
		_header = "GeneralException";
		_info = "Something went wrong!";
	}
	
	_GeneralException(const std::string &header, const std::string &info) throw() {
		_header = header;
		_info = info;
	}
	
	virtual const char* what() const throw() {
		return((_header + std::string(": ") + _info).c_str());
	}
	
	virtual ~_GeneralException() throw() {}
	
protected:
	std::string _header;
	std::string _info;
};

class _SDLException : public _GeneralException {
public:
	_SDLException() throw() : _GeneralException("SDLException", SDL_GetError()) {}
	_SDLException(const std::string &header) throw() 
	: _GeneralException(header, SDL_GetError()) {}
	
	virtual ~_SDLException() throw() {SDL_ClearError();}
};

#endif //__EXCEPTIONS__
