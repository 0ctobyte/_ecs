#ifndef __COMPONENT__
#define __COMPONENT__

#include <stdint.h>

#include <Vector.h>

class Entity;
class EntityManager;

typedef enum {
	C_NONE=1,
	C_POSITION=0,
	C_MAX
} _COMPONENTID;

class _Component {
public:
	_Component();
	virtual ~_Component() {}
	
	uint32_t id();
	_COMPONENTID cid();
	
protected:
	uint32_t _id;
	_COMPONENTID _cid;
	uint32_t _entity;
	
	friend class EntityManager;
};

struct _CPosition : public _Component {
	_CPosition(_Vector=_Vector());
	_Vector position;
};
	

#endif //__COMPONENT__
