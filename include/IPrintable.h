// Sekhar Bhattacharya

#ifndef __IPRINTABLE__
#define __IPRINTABLE__

#include <sstream>

class _IPrintable {
public:
	virtual ~_IPrintable() {}
	virtual const std::string toString() const = 0;	
};

#endif //__IPRINTABLE__
