#ifndef __ENTITYCOMPONENTMANAGER__
#define __ENTITYCOMPONENTMANAGER__

#include <Entity.h>

class _EntityComponentManager {
public:
	_EntityComponentManager();

private:
	std::vector<_Entity> _entities;
	std::vector<_Component> _components;
};

#endif //__ENTITYCOMPONENTMANAGER__
