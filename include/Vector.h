// Sekhar Bhattacharya

#ifndef __VECTOR__
#define __VECTOR__

#include <OpenGL.h>
#include <IPrintable.h>
#include <Exceptions.h>

typedef struct _Vector3 _Vertex;
typedef struct _Vector3 _Vector;

struct _Vector3 : public _IPrintable {
	float x, y, z;
	
	_Vector3(float x=0, float y=0, float z=0);
	_Vector3(const _Vector3&);
	
	float distance(const _Vector3&) const;
	_Vector3 normalize() const;
	float length() const;
	float angle(const _Vector3&) const;
	_Vector3 crossProduct(const _Vector3&) const;
	_Vector3 operator+(const _Vector3&) const;
	_Vector3 operator-(const _Vector3&) const;
	float operator*(const _Vector3&) const;
	_Vector3 operator*(float) const;
	_Vector3 operator-() const;
	_Vector3& operator=(const _Vector3&);
	bool operator==(const _Vector3&) const;
	bool operator!=(const _Vector3&) const;
	_Vector3 operator+=(const _Vector3&);
	_Vector3 operator-=(const _Vector3&);
	_Vector3 operator*=(float);
	_Vector3 operator/=(float);
	virtual const std::string toString() const;
};

struct _Vector4 : public _IPrintable {
	float x, y, z, w;
	
	_Vector4(float x=0, float y=0, float z=0, float w=0);
	_Vector4(const _Vector4&);
	_Vector4(const _Vector3&);
	
	float distance(const _Vector4&) const;
	_Vector4 normalize() const;
	float length() const;
	float angle(const _Vector4&) const;
	_Vector4 operator+(const _Vector4&) const;
	_Vector4 operator-(const _Vector4&) const;
	float operator*(const _Vector4&) const;
	_Vector4 operator*(float) const;
	_Vector4 operator-() const;
	_Vector4& operator=(const _Vector4&);
	bool operator==(const _Vector4&) const;
	bool operator!=(const _Vector4&) const;
	_Vector4 operator+=(const _Vector4&);
	_Vector4 operator-=(const _Vector4&);
	_Vector4 operator*=(float);
	_Vector4 operator/=(float);
	_Vector3 toVector3() const;
	virtual const std::string toString() const;
};

#endif //__VECTOR__
