// Sekhar Bhattacharya

#ifndef __APPLICATION__
#define __APPLICATION__

#include <SDL.h>

#include <Exceptions.h>

class _Application {
public:
	static _Application& getInstance();
	
	void init();
	int run();
	void stop();
	
private:
	_Application();
	_Application(const _Application&);
	_Application& operator=(const _Application&);
	~_Application();
	
	void destroy();
	
	SDL_Window *_window;
	SDL_GLContext _glContext;
	bool _go;
};

#endif //__APPLICATION__
