#ifndef __ENTITY__
#define __ENTITY__

#include <vector>

#include <Component.h>

class _EntityManager;

class _Entity {
public:
	_Entity();
	virtual ~_Entity() {}
	
	uint32_t id();
	
protected:
	uint32_t _id;
	std::vector<uint32_t> _components;
	
	friend class _EntityManager;
};

#endif //__ENTITY__
