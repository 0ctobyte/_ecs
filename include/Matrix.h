// Sekhar Bhattacharya

#ifndef __MATRIX__
#define __MATRIX__

#include <OpenGL.h>
#include <IPrintable.h>
#include <Exceptions.h>
#include <Vector.h>

typedef struct _Matrix44 _Matrix;

struct _Matrix44 : public _IPrintable {
	float m[16];
	
	_Matrix44();
	_Matrix44(const _Matrix44&);
	
	_Matrix44& loadIdentity();
	_Matrix44& frustum(float left, float right, float bottom, float top,
		float znear, float zfar);
	_Matrix44& perspective(float fovy, float faspect, float znear, float zfar);
	_Matrix44& orthographic(float left, float right, float bottom, float top, 
		float znear, float zfar);
	_Matrix44& translate(float x, float y, float z);
	_Matrix44& translate(const _Vector4&);
	_Matrix44 untranslate() const;
	_Matrix44 transpose() const;
	_Matrix44 inverse() const;
	float cofactor(GLuint column, GLuint row) const;
	float determinant() const;
	_Vector4 operator*(const _Vector4&) const;
	_Matrix44 operator*(const _Matrix44&) const;
	_Matrix44& operator=(const _Matrix44&);
	_Matrix44& operator*=(const _Matrix44&);
	virtual const std::string toString() const;
};

#endif //__MATRIX__
