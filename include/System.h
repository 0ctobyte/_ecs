#ifndef __SYSTEM__
#define __SYSTEM__

#include <stdint.h>

typedef enum {
	S_NONE=-1,
	S_MAX
} _SYSTEMID;

class _System {
public:
	_System();
	virtual ~_System() {}
	
	uint32_t id();
	_SYSTEMID sid();

private:
	uint32_t _id;
	_SYSTEMID _sid;
};


#endif //__SYSTEM__
