#include <Entity.h>

_Entity::_Entity() {
	_id = 0;
	_components.resize(C_MAX);
}

uint32_t _Entity::id() {
	return(_id);
}
