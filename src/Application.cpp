#include <SDL_log.h>
#include <SDL_assert.h>

#include <OpenGL.h>
#include <Application.h>

_Application::_Application() {
}

_Application::~_Application() {
	destroy();
}

_Application& _Application::getInstance() {
	static _Application app;
	return app;
}

void _Application::init() {
	SDL_Log("app init");
	try {
		if(SDL_Init(SDL_INIT_VIDEO) < 0) throw _SDLException("SDL_Init Error");
		
		// Use OpenGL 3.2 Core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		
		// Create SDL window
		SDL_Log("Creating SDL Window");
		_window = SDL_CreateWindow("galaxy", SDL_WINDOWPOS_UNDEFINED, 
			SDL_WINDOWPOS_UNDEFINED, 512, 512, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if(_window == NULL) throw _SDLException("SDL_CreateWindow Error");
		
		// Create OpenGL context in SDL window
		SDL_Log("Creating OpenGL context");
		_glContext = SDL_GL_CreateContext(_window);
		if(_glContext == NULL) throw _SDLException("SDL_GL_CreateContext Error");
		
		// Use VSYNC
		SDL_Log("Enabling VSYNC");
		if(SDL_GL_SetSwapInterval(1) < 0)
			throw _SDLException("Cannot set VSYNC! SDL_GL_SetSwapInterval Error");
			
		SDL_Log("OpenGL settings init");
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	} catch(_SDLException &e) {
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, e.what());
	}
}

int _Application::run() {
	_go = true;
	
	SDL_Event e;
	while(_go) {
		while(SDL_PollEvent(&e) != 0) {
			if(e.type == SDL_QUIT) {
				stop();
			}
		}
		glClear(GL_COLOR_BUFFER_BIT);
		SDL_GL_SwapWindow(_window);
	}
	
	destroy();
	return 0;
}
	
void _Application::stop() {
	_go = false;
}

void _Application::destroy() {
	// Destroy SDL window
	SDL_DestroyWindow(_window);
	_window = NULL;
	
	// Quit SDL subsystems
	SDL_Quit();
}
