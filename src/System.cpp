#include <System.h>

_System::_System() {
	_sid = S_NONE;
	_id = 0;
}

uint32_t _System::id() {
	return(_id);
}

_SYSTEMID _System::sid() {
	return(_sid);
}
