#include <Application.h>

int main(int argc, char **argv) {
	_Application& app = _Application::getInstance();
	app.init();
	return app.run();
}
