#include <Component.h>

_Component::_Component() {
	_cid = C_NONE;
	_id = 0;
	_entity = 0;
}

uint32_t _Component::id() {
	return(_id);
}

_COMPONENTID _Component::cid() {
	return(_cid);
}

_CPosition::_CPosition(_Vector v) : position(v) {
	_id = C_POSITION;
}

