// Sekhar Bhattacharya

#include <cmath>
#include <stdint.h>
#include <iomanip>

#include <Matrix.h>

_Matrix44::_Matrix44() {
	for(uint32_t i=0; i<16; i++) m[i]=((i%5==0) ? 1.0f : 0.0f);
}

_Matrix44::_Matrix44(const _Matrix44 &A) {
	*this = A;
}

_Matrix44& _Matrix44::loadIdentity() {
	for(uint32_t i=0; i<16; i++) m[i]=((i%5==0) ? 1.0f : 0.0f);
	return(*this);
}

_Matrix44& _Matrix44::frustum(float left, float right, float bottom, 
		float top, float znear, float zfar) {
	_Matrix44 A;
	A.m[0] = (2.0f*znear)/(right-left);
	A.m[5] = (2.0f*znear)/(top-bottom);
	A.m[8] = (right+left)/(right-left);
	A.m[9] = (top+bottom)/(top-bottom);
	A.m[10] = -((zfar+znear)/(zfar-znear));
	A.m[11] = -1.0f;
	A.m[14] = -((2.0f*zfar*znear)/(zfar-znear));
	A.m[15] = 0.0f;
	(*this)*=A;
	return(*this);
}

_Matrix44& _Matrix44::perspective(float fovy, float faspect, float znear,
		float zfar) {
	_Matrix44 A;
	float f = 1.0f/(tan((fovy*(M_PI/180.0f))/2.0f));
	A.m[0]=f/faspect;
	A.m[5]=f;
	A.m[10]=(zfar+znear)/(znear-zfar);
	A.m[11]=-1.0f;
	A.m[14]=(2.0f*zfar*znear)/(znear-zfar);
	A.m[15]=0.0f;
	(*this)*=A;
	return(*this);
}

_Matrix44& _Matrix44::orthographic(float left, float right, float bottom,
		float top, float znear, float zfar) {
	_Matrix44 A;
	A.m[0] = 2.0f/(right-left);
	A.m[5] = 2.0f/(top-bottom);
	A.m[10] = -2.0f/(zfar-znear);
	A.m[12] = -((right+left)/(right-left));
	A.m[13] = -((top+bottom)/(top-bottom));
	A.m[14] = -((zfar+znear)/(zfar-znear));
	(*this)*=A;
	return(*this);
}

_Matrix44& _Matrix44::translate(float x, float y, float z) {
	_Matrix44 A;
	A.m[12] = x, A.m[13] = y, A.m[14] = z;
	(*this)*=A;
	return(*this);
}

_Matrix44& _Matrix44::translate(const _Vector4 &v) {
	_Matrix44 A;
	A.m[12] = v.x, A.m[13] = v.y, A.m[14] = v.z;
	(*this)*=A;
	return(*this);
}

_Matrix44 _Matrix44::untranslate() const {
	_Matrix44 A = (*this);
	A.m[12] = 0.0f, A.m[13] = 0.0f, A.m[14] = 0.0f;
	return(A);
}

_Matrix44 _Matrix44::transpose() const {
	_Matrix44 A;
	for(uint32_t c=0; c<4; c++)
		for(uint32_t r=0; r<4; r++)
			A.m[r*4+c]=m[c*4+r];
	return(A);
}

_Matrix44 _Matrix44::inverse() const {
	_Matrix44 A;
	float det = determinant();
	if(fabs(det) < 0.000001f) return(A);
	for(uint32_t c=0; c<4; c++)
		for(uint32_t r=0; r<4; r++)
			A.m[c*4+r]=cofactor(c, r)/det;
	return(A.transpose());
}

float _Matrix44::cofactor(uint32_t column, uint32_t row) const {
	if((int32_t)column<0 || column>3 || (int32_t)row<0 || row>3) return(0.0f);
	float cofactor = 0.0f;
	for(uint32_t c=0; c<4; c++)
	{
		if(c==column) continue;
		float pfactor=1, nfactor=1;
		for(int32_t pcc=c, ncc=c, r=0; r<4; r++)
		{
			if(r==(int32_t)row) continue;
			if(pcc==(int32_t)column) pcc++;
			if(pcc>3) pcc=(((int32_t)column==0) ? 1 : 0);
			if(ncc==(int32_t)column) ncc--;
			if(ncc<0) ncc=(((int32_t)column==3) ? 2 : 3);
			pfactor*=m[pcc*4+r];
			nfactor*=m[ncc*4+r];
			pcc++, ncc--;
		}
		cofactor+=pfactor-nfactor;
	}
	return(((row+column)%2==0) ? cofactor : -1*cofactor);
}

float _Matrix44::determinant() const {
	//Basically, we have to take an arbitrary row (or column), in this case we
	//took the fourth row of the matrix, and multiply each element in that row
	//by it's signed cofactor, which is found by calculating the determinant of
	//the minor of that element, and then summing up all the values.
	return(m[3]*cofactor(0, 3)+m[7]*cofactor(1, 3)+m[11]*cofactor(2, 3)
			+m[15]*cofactor(3, 3));
}

_Vector4 _Matrix44::operator*(const _Vector4 &v) const {
	_Vector4 u;
	u.x = m[0]*v.x+m[4]*v.y+m[8]*v.z+m[12]*v.w;
	u.y = m[1]*v.x+m[5]*v.y+m[9]*v.z+m[13]*v.w;
	u.z = m[2]*v.x+m[6]*v.y+m[10]*v.z+m[14]*v.w;
	u.w = m[3]*v.x+m[7]*v.y+m[11]*v.z+m[15]*v.w;
	return(u);
}

_Matrix44 _Matrix44::operator*(const _Matrix44 &A) const {
	_Matrix44 a;
	for(uint32_t i=0, offset=0, ii=0; i<16; i++, ii++)
	{
		a.m[i]=0, offset=((i%4==0) ? i : offset), ii=((ii>3) ? 0 : ii);
		for(uint32_t j=0, mult=0; j<4; mult++, j++)
		{
			a.m[i]+=m[ii+mult*4]*A.m[offset+j];
		}
	}
	return(a);
}

_Matrix44& _Matrix44::operator=(const _Matrix44 &A) {
	memmove(m, A.m, 16*sizeof(float));
	return(*this);
}

_Matrix44& _Matrix44::operator*=(const _Matrix44 &A) {
	(*this) = (*this)*A;
	return(*this);
}

const std::string _Matrix44::toString() const {
	std::ostringstream s;

	s<<std::left;
	s<<"| "<<std::setw(10)<<m[0]<<std::setw(10)<<m[4]<<std::setw(10);
	s<<m[8]<<std::setw(10)<<m[12]<<" |"<<std::endl; 

	s<<"| "<<std::setw(10)<<m[1]<<std::setw(10)<<m[5]<<std::setw(10);
   	s<<m[9]<<std::setw(10)<<m[13]<<" |"<<std::endl;

	s<<"| "<<std::setw(10)<<m[2]<<std::setw(10)<<m[6]<<std::setw(10);
	s<<m[10]<<std::setw(10)<<m[14]<<" |"<<std::endl; 

	s<<"| "<<std::setw(10)<<m[3]<<std::setw(10)<<m[7]<<std::setw(10);
	s<<m[11]<<std::setw(10)<<m[15]<<" |"; 

	return(s.str());
}
