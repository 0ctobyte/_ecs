#include <cmath>
#include <iomanip>

#include <Vector.h>

_Vector3::_Vector3(float x, float y, float z) : x(x), y(y), z(z) {
}

_Vector3::_Vector3(const _Vector3 &v) {
	*this = v;
}

float _Vector3::distance(const _Vector3 &v) const {
	return((*this - v).length());
}

_Vector3 _Vector3::normalize() const {
	float k = this->length();
	return(_Vector3(x/k, y/k, z/k));
}

float _Vector3::length() const {
	return(sqrt(x*x+y*y+z*z));
}

float _Vector3::angle(const _Vector3 &v) const {
	return(acos(((*this)*v)/(this->length()*v.length())));
}

_Vector3 _Vector3::crossProduct(const _Vector3 &v) const {
	return(_Vector3(y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x));
}

_Vector3 _Vector3::operator+(const _Vector3 &v) const {
	return(_Vector3(x+v.x, y+v.y, z+v.z));
}

_Vector3 _Vector3::operator-(const _Vector3 &v) const {
	return(_Vector3(x-v.x, y-v.y, z-v.z));
}

float _Vector3::operator*(const _Vector3 &v) const {
	return(x*v.x+y*v.y+z*v.z);
}

_Vector3 _Vector3::operator*(float k) const {
	return(_Vector3(x*k, y*k, z*k));
}

_Vector3 _Vector3::operator-() const {
	return(_Vector3(-x, -y, -z));
}

_Vector3& _Vector3::operator=(const _Vector3 &v) {
	x=v.x, y=v.y, z=v.z;
	return(*this);
}

bool _Vector3::operator==(const _Vector3 &v) const {
	return(x==v.x && y==v.y && z==v.z);
}

bool _Vector3::operator!=(const _Vector3 &v) const {
	return(x!=v.x || y!=v.y || z!=v.z);
}

_Vector3 _Vector3::operator+=(const _Vector3 &v) {
	x+=v.x, y+=v.y, z+=v.z;
	return(*this);
}

_Vector3 _Vector3::operator-=(const _Vector3 &v) {
	x-=v.x, y-=v.y, z-=v.z;
	return(*this);
}

_Vector3 _Vector3::operator*=(float k) {
	x*=k, y*=k, z*=k;
	return(*this);
}

_Vector3 _Vector3::operator/=(float k) {
	x/=k, y/=k, z/=k;
	return(*this);
}

const std::string _Vector3::toString() const {
	std::ostringstream s(std::ostringstream::out);
	s<<std::left;
	s<<"["<<std::setw(10)<<x<<", "<<std::setw(10)<<y<<", "<<std::setw(10)<<z<<"]";
	return(s.str());
}

_Vector4::_Vector4(float x, float y, float z, float w) : x(x), y(y), z(z), 
	w(w) {
}

_Vector4::_Vector4(const _Vector4 &v) {
	*this = v;
}

_Vector4::_Vector4(const _Vector3 &v) : x(v.x), y(v.y), z(v.z), w(1) {
}

float _Vector4::distance(const _Vector4 &v) const {
	return((*this - v).length());
}

_Vector4 _Vector4::normalize() const {
	float k = this->length();
	return(_Vector4(x/k, y/k, z/k, w/k));
}

float _Vector4::length() const {
	return(sqrt(x*x+y*y+z*z+w*w));
}

float _Vector4::angle(const _Vector4 &v) const {
	return(acos(((*this)*v)/(this->length()*v.length())));
}

_Vector4 _Vector4::operator+(const _Vector4 &v) const {
	return(_Vector4(x+v.x, y+v.y, z+v.z, w+v.w));
}

_Vector4 _Vector4::operator-(const _Vector4 &v) const {
	return(_Vector4(x-v.x, y-v.y, z-v.z, w-v.w));
}

float _Vector4::operator*(const _Vector4 &v) const {
	return(x*v.x+y*v.y+z*v.z+w*v.w);
}

_Vector4 _Vector4::operator*(float k) const {
	return(_Vector4(x*k, y*k, z*k, w*k));
}

_Vector4 _Vector4::operator-() const {
	return(_Vector4(-x, -y, -z, -w));
}

_Vector4& _Vector4::operator=(const _Vector4 &v) {
	x=v.x, y=v.y, z=v.z, w=v.w;
	return(*this);
}

bool _Vector4::operator==(const _Vector4 &v) const {
	return(x==v.x && y==v.y && z==v.z && w==v.w);
}

bool _Vector4::operator!=(const _Vector4 &v) const {
	return(x!=v.x || y!=v.y || z!=v.z || w!=v.w);
}

_Vector4 _Vector4::operator+=(const _Vector4 &v) {
	x+=v.x, y+=v.y, z+=v.z, w+=v.w;
	return(*this);
}

_Vector4 _Vector4::operator-=(const _Vector4 &v) {
	x-=v.x, y-=v.y, z-=v.z, w-=v.w;
	return(*this);
}

_Vector4 _Vector4::operator*=(float k) {
	x*=k, y*=k, z*=k, w*=k;
	return(*this);
}

_Vector4 _Vector4::operator/=(float k) {
	x/=k, y/=k, z/=k, w/=k;
	return(*this);
}

_Vector3 _Vector4::toVector3() const {
	return(_Vector3(x, y, z));
}

const std::string _Vector4::toString() const {
	std::ostringstream s(std::ostringstream::out);
	s<<std::left;
	s<<"["<<std::setw(10)<<x<<", "<<std::setw(10)<<y<<", "<<std::setw(10)<<z;
	s<<", "<<std::setw(10)<<w<<"]";
	return(s.str());
}
